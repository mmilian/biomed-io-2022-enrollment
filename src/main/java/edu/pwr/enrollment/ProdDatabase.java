package edu.pwr.enrollment;

public class ProdDatabase implements Database {

    private final DatabaseConf dbConf;
    private final String dbType = "Prod-Database";

    public ProdDatabase(DatabaseConf databaseConf) {
        this.dbConf = databaseConf;
    }

    @Override
    public void printDb() {
        System.out.println(this);
    }

    @Override
    public String toString() {
        return "ProdDatabase{" +
                "dbConf=" + dbConf +
                ", dbType='" + dbType + '\'' +
                '}';
    }

}
package edu.pwr.enrollment;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class AppLoader implements CommandLineRunner {

    final StudentRepo studentRepo;
    final CourseRepo courseRepo;
    final StudentService studentService;

    public AppLoader(StudentRepo studentRepo, CourseRepo courseRepo,StudentService studentService) {
        this.studentRepo = studentRepo;
        this.courseRepo = courseRepo;
        this.studentService = studentService;
    }

    @Override
    public void run(String... args) throws Exception {
        studentRepo.save(new Student("A1", "Jan Nowak"));
        studentRepo.save(new Student("A2", "Piotr Kowal"));
        studentRepo.save(new Student("A3", "Krzysztow Jan"));
        courseRepo.save(new Course("X1", "Inżynieria Oprogramowania"));
        courseRepo.save(new Course("X2", "Wstęp do programowania"));
        studentService.addStudentToCourse("A1", "X1");
        studentRepo.findById("A1").ifPresent(s -> System.out.println(s));
        System.out.println("Loader is loading.......");

    }
}

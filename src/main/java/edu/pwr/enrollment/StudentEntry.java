package edu.pwr.enrollment;

import javax.persistence.*;

@Entity
public class StudentEntry {

    @EmbeddedId
    StudentEntryKey id;

    @ManyToOne
    @MapsId("studentId")
    @JoinColumn(name = "student_id")
    Student student;

    @ManyToOne
    @MapsId("courseId")
    @JoinColumn(name = "course_id")
    Course course;

    String datetime;

    public StudentEntry() {

    }

    public StudentEntry(Student student, Course course, String datetime) {
        this.id = new StudentEntryKey(student.getId(), course.getId());
        this.student = student;
        this.course = course;
        this.datetime = datetime;
    }

    public StudentEntryKey getId() {
        return id;
    }

    public void setId(StudentEntryKey id) {
        this.id = id;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    @Override
    public String toString() {
        return "StudentEntry{" +
                " course=" + course +
                ", datetime='" + datetime + '\'' +
                '}';
    }
}

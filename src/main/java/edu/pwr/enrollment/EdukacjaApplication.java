package edu.pwr.enrollment;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.Scanner;

@SpringBootApplication
public class EdukacjaApplication implements ApplicationContextAware {

	private static ApplicationContext appCtx;

	public static void main(String[] args) {
		System.out.println("Edukacja is about to load");
		SpringApplication.run(EdukacjaApplication.class, args);
		System.out.println("Edukacja is loaded");
		StudentRepo studentRepo = appCtx.getBean(StudentRepo.class);
//		System.out.println(studentRepo.findAll());

	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		appCtx = applicationContext;
	}
}

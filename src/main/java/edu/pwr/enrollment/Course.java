package edu.pwr.enrollment;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class Course {

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Id
    String id;
    @Column
    String name;

    @ManyToMany(mappedBy = "courses")
    Set<Student> students = new HashSet();

    public Course(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public Course() {
    }


    public Set<Student> getStudents() {
        return students;
    }

    public void setStudents(Set<Student> students) {
        this.students = students;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Course course = (Course) o;
        return id.equals(course.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

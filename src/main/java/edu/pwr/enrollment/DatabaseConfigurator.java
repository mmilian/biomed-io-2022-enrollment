package edu.pwr.enrollment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class DatabaseConfigurator {

    @Autowired
    DatabaseConf databaseConf;

    @Bean
    @Profile("dev")
    public Database newDevDatabase() {
        return new DevDatabase(databaseConf);
    }

    @Bean
    @Profile("prod")
    public Database newProdDatabase() {
        return new ProdDatabase(databaseConf);
    }


}

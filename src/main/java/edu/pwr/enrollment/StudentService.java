package edu.pwr.enrollment;

import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
@Transactional
public class StudentService {

    final StudentRepo studentRepo;
    final CourseRepo courseRepo;

    public StudentService(StudentRepo studentRepo, CourseRepo courseRepo) {
        this.studentRepo = studentRepo;
        this.courseRepo = courseRepo;
    }

    void addStudentToCourse(String studentId, String courseId) {
        studentRepo.findById(studentId).ifPresent(student -> {
            courseRepo.findById(courseId).ifPresent(course -> {
                        student.getCourses().add(course);
                        studentRepo.save(student);
                    }
            );
        });
    }

}

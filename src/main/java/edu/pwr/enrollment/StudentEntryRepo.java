package edu.pwr.enrollment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface StudentEntryRepo extends JpaRepository<StudentEntry, String> {
}

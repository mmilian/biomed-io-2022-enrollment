package edu.pwr.enrollment;

public class DevDatabase implements Database {



    private final DatabaseConf dbConf;
    private final String dbType = "Dev-Database";

    public DevDatabase(DatabaseConf databaseConf) {
        this.dbConf = databaseConf;
    }

    @Override
    public void printDb() {
        System.out.println(this);
    }

    @Override
    public String toString() {
        return "DevDatabase{" +
                "dbConf=" + dbConf +
                ", dbType='" + dbType + '\'' +
                '}';
    }
}
